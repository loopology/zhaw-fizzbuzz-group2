from fb_tokens.fiz_buzz_tokens import get_fizz_buzz_token


def test_token_no_1_is_1():
    assert "1" == get_fizz_buzz_token(1)


def test_token_no_2_is_2():
    assert "2" == get_fizz_buzz_token(2)


def test_token_no_3_is_fizz():
    assert "fizz" == get_fizz_buzz_token(3)


def test_token_no_5_is_buzz():
    assert "buzz" == get_fizz_buzz_token(5)


def test_token_no_6_is_fizz():
    assert "fizz" == get_fizz_buzz_token(6)


def test_token_no_10_is_buzz():
    assert "buzz" == get_fizz_buzz_token(10)


def test_token_no_15_is_fizz_buzz():
    assert "fizz_buzz" == get_fizz_buzz_token(15)


def test_token_bs():
    assert get_fizz_buzz_token(-1) is None

