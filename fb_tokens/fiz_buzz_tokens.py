def get_fizz_buzz_token(param: int):
    fizz_number = 3
    buzz_number = 5
    answer = None

    def is_divisible(num, by):
        return num % by == 0

    if param < 1:
        answer = None
    elif is_divisible(param, fizz_number) and is_divisible(param, buzz_number):
        answer = "fizz_buzz"
    elif is_divisible(param, fizz_number):
        answer = "fizz"
    elif is_divisible(param, buzz_number):
        answer = "buzz"
    else:
        answer = str(param)
    return answer
